/**
 * @module application core module
 */

import './modules/map.js';
import './modules/cities-layer/main.js';

import i18n from 'i18n';
import $ from 'jquery';

/**
 * DOM: change language handler
 */
$('#lang').on('change', function () {
    i18n.lang = $(this).val();
}).trigger('change');