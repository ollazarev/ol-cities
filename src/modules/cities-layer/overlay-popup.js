/**
 * @module include popup for cities
 */

import ol from 'ol';
import $ from 'jquery';
import map from '../map.js';
import {vectorLayer} from './cities-layer.js';
import popupHtml from './overlay-popup.html';
import './overlay-popup.css';
import i18n from 'i18n';
import emitter from 'emitter';

/**
 * elements that make up the popup.
 */
let container = $(popupHtml);
let content = container.find('#popup-content');
let closer = container.find('#popup-closer');


/**
 * create an overlay to anchor the popup to the map.
 */
let overlayPopup = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
    element: container.get(0),
    autoPan: true,
    autoPanAnimation: {
        duration: 250
    },
    offset: [0, -15]
}));

map.addOverlay(overlayPopup);


/**
 * add a click handler to hide the popup.
 * @return {boolean} Don't follow the href.
 */
closer.on('click', function() {
    overlayPopup.setPosition(undefined);
    closer.blur();
    return false;
});

emitter.on(emitter.EVENT_CHANGE_LANGUAGE, function () {
    closer.trigger('click');
});

/**
 * show popup when hovering over the city
 */
map.on('pointermove', function(browserEvent) {
    let pixel = browserEvent.pixel;
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
        content.text(`${i18n.t('translation.population')}: ${feature.get('population')}`);
        overlayPopup.setPosition(feature.getGeometry().getCoordinates());
    }, {
        layerFilter: function (layer) {
            return layer === vectorLayer;
        }
    });
});

export default overlayPopup;