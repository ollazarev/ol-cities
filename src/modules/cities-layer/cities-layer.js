/**
 * @module include a layer with cities to the map
 */

import ol from 'ol';
import map from '../map.js';
import i18n from 'i18n';
import emitter from 'emitter';

/**
 * cities geoJson
 */
let geojsonObject = {
    'type': 'FeatureCollection',
    "features": [{
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [-0.1275, 51.507222],
        },
        "properties": {
            "id": 1,
            "population": '8,787,892',
        }
    }, {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [2.3508, 48.8567]
        },
        "properties": {
            "id": 5,
            "population": '2,229,621',
        }
    }, {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [-122.4195, 37.7749],
        },
        "properties": {
            "id": 60,
            "population": '852,469',
        }
    }, {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [139.6917, 35.6895],
        },
        "properties": {
            "id": 70,
            "population": '13,617,445',
        }
    }, {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [116.383333, 39.916667],
        },
        "properties": {
            "id": 80,
            "population": '21,700,000',
        }
    }]
};

let vectorSource = new ol.source.Vector({
    features: (new ol.format.GeoJSON()).readFeatures(geojsonObject, {
        featureProjection: 'EPSG:3857',
    })
});

emitter.on(emitter.EVENT_CHANGE_LANGUAGE, function () {
    vectorSource.changed();
});

/**
 * layer with cities
 * @type {ol.layer.Vector}
 */
let vectorLayer = new ol.layer.Vector({
    source: vectorSource,
    style: featureStyle,
});

map.addLayer(vectorLayer);

/**
 * get feature style
 * @param {ol.Feature} feature
 * @returns {ol.style.Style}
 */
function featureStyle(feature) {
    let cityId = feature.get('id');
    return new ol.style.Style({
        image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
                color: [0, 153, 255, 1]
            }),
            stroke: new ol.style.Stroke({
                color: [255, 255, 255, 0.75],
                width: 1.5
            })
        }),
        text: new ol.style.Text({
            text: i18n.t(`city.${cityId}`),
            font: 'bold 14px sans-serif',
            offsetY: -15,
        }),
    });
}

/**
 * go to the link when clicking on a city
 */
map.on('click', function(browserEvent) {
    let pixel = browserEvent.pixel;
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
        let cityId = feature.get('id');
        let name = i18n.t(`city.${cityId}`);
        openInNewTab(`https://${i18n.t('wiki-domain')}.wikipedia.org/wiki/${name}`);
    }, {
        layerFilter: function (layer) {
            return layer === vectorLayer;
        }
    });
});

function openInNewTab(url) {
    let win = window.open(url, '_blank');
    win.focus();
}

/**
 * color text when hovering over the city
 * @type {ol.interaction.Select}
 */
let highlighter = new ol.interaction.Select({
    condition: ol.events.condition.pointerMove,
    layers: [vectorLayer],
    style: function (feature) {
        let style = featureStyle(feature);
        style.getText().setFill(new ol.style.Fill({
            color: [255, 0, 0, 1]
        }));
        return style;
    }
});
map.addInteraction(highlighter);

export {vectorLayer, featureStyle};