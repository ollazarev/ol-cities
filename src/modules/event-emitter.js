/**
 * @module event emitter creation
 */

import Emitter from 'event-emitter-es6';

const EVENT_CHANGE_LANGUAGE = Symbol(); //change language event

class DefaultEmitter extends Emitter {
    constructor () {
        super();
    }

    get EVENT_CHANGE_LANGUAGE () {
        return EVENT_CHANGE_LANGUAGE;
    }
}

export default new DefaultEmitter();