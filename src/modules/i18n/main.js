/**
 * @module internationalization of the application
 */

import emitter from '../event-emitter';

let i18n = {

    _lang: null,

    /**
     * internationalization settings
     */
    _settings: {
        "en": {
            "city": {
                "1": "London",
                "5": "Paris",
                "60": "San-Francisco",
                "70": "Tokyo",
                "80": "Beijing",
            },
            "translation": {
                "population": "Population",
            },
            "wiki-domain": "en",
        },
        "ru": {
            "city": {
                "1": "Лондон",
                "5": "Париж",
                "60": "Сан-Франциско",
                "70": "Токио",
                "80": "Пекин",
            },
            "translation": {
                "population": "Население",
            },
            "wiki-domain": "ru",
        },
    },

    /**
     * set language
     * @param {string} val short name of the language
     */
    set lang (val) {
        if (this._lang === val) {
            return;
        }
        this._lang = val;
        emitter.emit(emitter.EVENT_CHANGE_LANGUAGE);
    },

    /**
     * get name for the current language
     * @param {string} path path to the appropriate setting
     * @returns {string}
     */
    t (path) {
        let _path = path.split('.');
        let result = this._settings[this._lang];
        for (let i = 0; i < _path.length; i++) {
            result = result[_path[i]];
        }
        return result;
    }
};

export default i18n;