"use strict";

var gulp = require('gulp'),
    rimraf = require('rimraf'),
    webpackStream = require('webpack2-stream-watch'),
    path = require('path');

var path_to = {
    dist: { //path to dist files
        html: 'dist/',
        js: 'dist/',
    },
    src: { //path to source files
        html: 'src/index.html',
        js: 'src/app.js',
    },
    clean: './dist'
};

gulp.task('html', function () {
    gulp.src(path_to.src.html)
        .pipe(gulp.dest(path_to.dist.html))
});

gulp.task("js", function () {
    return gulp.src(path_to.src.js)
        .pipe(webpackStream({   //es2015 -> es5
            module: {
                rules: [
                    {
                        test: /\.jsx?$/,
                        exclude: /node_modules/,
                        loader: "babel-loader",
                        options: {
                            presets: ["es2015"]
                        },
                    },
                    {
                        test: /\.css$/,
                        use: [
                            'style-loader',
                            'css-loader'
                        ]
                    },
                    {
                        test: /\.html$/,
                        exclude: /node_modules/,
                        use: [
                            'html-loader'
                        ]
                    }
                ]
            },
            output: {
                filename: 'app.js',
            },
            resolve: {
                alias: {
                    ol$: path.resolve(__dirname, './node_modules/openlayers/dist/ol.js'),
                    i18n$: path.resolve(__dirname, './src/modules/i18n/main.js'),
                    emitter$: path.resolve(__dirname, './src/modules/event-emitter')
                },
            },
        }))
        .pipe(gulp.dest(path_to.dist.js));
});

gulp.task('clean', function (cb) {
    rimraf(path_to.clean, cb);
});

gulp.task('default', ['clean'], function() {
    gulp.start('html');
    gulp.start('js');
});